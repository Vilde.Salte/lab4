package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    // feltvariable
    private CellState[][] cellState;
    private int cols ;
    private int rows;

    // konstrøktør
    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.cols = columns;
        cellState = new CellState[this.rows][this.cols];
        for(int row = 0 ; row < rows ;  row++){
            for (int col = 0; col < columns ; col ++)
                cellState[row][col] = initialState;
        }
        // TODO Auto-generated constructor stub

	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return cols;
    }
    public boolean validValue(int rows, int colums){
        if ((rows >= 0) && (rows < this.rows)){
            return (colums >= 0) && (colums < this.cols);
            }
        else{
            return false;
        }
        }
    @Override
    public void set(int row, int column, CellState element) {
        if (!validValue(row, column)) {
            throw new IndexOutOfBoundsException();
        }
        cellState[row][column] = element;


    }

    @Override
    public CellState get(int row, int column) {
        if (validValue(row,column)){
            return cellState[row][column];
    }
        else{
            throw new IllegalArgumentException();
        }
    }
    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        IGrid newGrid = new CellGrid(this.rows,this.cols,CellState.DEAD);
        for (int row = 0; row< this.rows; row++){
            for (int col = 0; col < this.cols; col++){
                newGrid.set(row,col,this.get(row,col));
            }
        }
        return  newGrid;
    }
    
}
